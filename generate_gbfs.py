import time
import json

class GenerateGBFS():

    def serialize(self, bikes):
        data = {}
        data["ttl"] = 60
        data["last_updated"] = int(time.time())
        data["data"] = {}
        data["data"]["bikes"] = list(map(lambda bike: bike.gbfs_dict(), bikes))

        return json.dumps(data)

