import requests
import psycopg2
import logging
import time
import queue
import threading
import generate_gbfs
import os
import configparser

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)

config = configparser.ConfigParser()
config.read('config.ini')

class BikeLocation():
    def __init__(self, bike_id, lat, lng):
        self.bike_id = bike_id
        self.lat = lat
        self.lng = lng

    def gbfs_dict(self):
        data = {}
        data["bike_id"] = self.bike_id
        data["lat"] = float(self.lat)
        data["lon"] = float(self.lng)
        data["is_reserved"] = 0
        data["is_disabled"] = 0
        return data

class DonkeyRepublicImporter():
    def __init__(self, token):
        self.token = token

    def import_bike(self, feed, bike):
        return BikeLocation(str(bike["id"]), 
            bike["latitude"], bike["longitude"])

    def import_json(self, feed, json):
        cycles = []
        for bike in json:
            cycles.append(self.import_bike(feed, bike))
        return cycles

    def get_bikes_at_hub(self, hub_id):
        url = 'https://stables.donkey.bike/api/public/availability/hubs/%s/bikes' % hub_id
        payload = {'location': '52.160105,5.382660', 'radius': '100000'}
        headers = {'X-Api-Key': self.token, 'Accept': 'application/com.donkeyrepublic.v2'}
        r = requests.get(url, params=payload, headers=headers)
        if r.status_code != 200:
            return []
        return self.import_json("donkey", r.json())

    def get_hubs(self): 
        url = 'https://stables.donkey.bike/api/public/availability/hubs'
        payload = {'location': '52.160105,5.382660', 'radius': '100000'}
        headers = {'X-Api-Key': self.token, 'Accept': 'application/com.donkeyrepublic.v2'}
        r = requests.get(url, params=payload, headers=headers)
        return r.json()

    def import_feed(self):
        cycles = []
        hubs = self.get_hubs()
        for hub in hubs:
            cycles += self.get_bikes_at_hub(hub["id"])

        return cycles



importer = DonkeyRepublicImporter(config['donkey']['token'])
gbfs_generator = generate_gbfs.GenerateGBFS()

conn_str = "dbname=deelfietsdashboard"
if "dev" in os.environ:
    conn_str = "dbname=deelfietsdashboard3"

if "ip" in os.environ:
    conn_str += " host={} ".format(os.environ['ip'])
if "password" in os.environ:
    conn_str += " user=deelfietsdashboard password={}".format(os.environ['password'])


conn = psycopg2.connect(conn_str)
cur = conn.cursor()

while True:
    start = time.time()

    logging.info("Start import.")
    result = importer.import_feed()
    number_of_bikes = len(result)
    logging.info("Completed import in %.2f, imported %s bikes." % ((time.time() - start), number_of_bikes))
    
    delta_time = 60 * 1 - (time.time() - start)
    
    json = gbfs_generator.serialize(result)
    stmt = """INSERT INTO raw_gbfs
        (feed, json)
        VALUES (%s, %s)
        ON CONFLICT (feed) DO UPDATE 
            SET json = excluded.json;
        """
    cur.execute(stmt, ("donkey", json))
    conn.commit()

    if delta_time > 0:
        time.sleep(delta_time)
